package com.example.doorweb.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class PersonCommand {
    private String firstName;
    private String lastName;
    private MultipartFile photoFile;
    private String email;
    private String sponsorName;
}
