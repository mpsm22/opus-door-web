package com.example.doorweb.commands;

import com.example.doorweb.model.UserType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@NoArgsConstructor
public class UserCommand {
    private Long id;
    private UserType userType;
    private String login;
    private String password;
}
