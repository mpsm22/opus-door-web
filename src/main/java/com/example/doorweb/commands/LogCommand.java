package com.example.doorweb.commands;

import lombok.Getter;
import lombok.AllArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class LogCommand {

    private final String personId;
    private final String encodedImage;
}
