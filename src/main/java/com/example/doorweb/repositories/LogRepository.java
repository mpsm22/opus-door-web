package com.example.doorweb.repositories;

import com.example.doorweb.model.Log;
import org.springframework.data.repository.CrudRepository;

public interface LogRepository extends CrudRepository<Log, Long> {

}
