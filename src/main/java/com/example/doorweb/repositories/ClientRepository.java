package com.example.doorweb.repositories;

import com.example.doorweb.model.Client;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepository extends CrudRepository<Client, Long> {
}
