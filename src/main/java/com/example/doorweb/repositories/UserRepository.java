package com.example.doorweb.repositories;

import com.example.doorweb.model.Person;
import com.example.doorweb.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

}
