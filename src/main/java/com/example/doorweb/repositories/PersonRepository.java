package com.example.doorweb.repositories;

import com.example.doorweb.model.Person;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepository extends CrudRepository<Person, Long> {

}
