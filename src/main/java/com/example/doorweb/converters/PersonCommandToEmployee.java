package com.example.doorweb.converters;

import com.example.doorweb.commands.PersonCommand;
import com.example.doorweb.model.Employee;
import com.example.doorweb.model.Person;
import com.example.doorweb.services.SlackService;
import com.sun.istack.Nullable;
import lombok.SneakyThrows;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Component
public class PersonCommandToEmployee implements Converter<PersonCommand, Person> {

    private final SlackService slackService;

    public PersonCommandToEmployee(SlackService slackService) {this.slackService = slackService;}

    @SneakyThrows
    @Synchronized
    @Nullable
    @Override
    public Employee convert(PersonCommand personCommand) {
        if (personCommand == null) {
            return null;
        }

        Employee employee = new Employee();
        employee.setFirstName(personCommand.getFirstName());
        employee.setLastName(personCommand.getLastName());
        employee.setPhoto(extractPhotoBytes(personCommand.getPhotoFile()));

        employee.setEmail(personCommand.getEmail());
        employee.setSlack(slackService.getUserId(personCommand.getEmail()));
        return employee;
    }

    private Byte[] extractPhotoBytes(MultipartFile photoFile) throws IOException {
        Byte[] photo = new Byte[photoFile.getBytes().length];
        int i = 0;
        for (Byte b : photoFile.getBytes()) {
            photo[i] = b;
            i++;
        }
        return photo;
    }
}
