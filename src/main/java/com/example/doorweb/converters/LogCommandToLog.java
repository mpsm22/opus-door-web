package com.example.doorweb.converters;

import com.example.doorweb.commands.LogCommand;
import com.example.doorweb.model.Log;
import com.example.doorweb.model.Person;
import com.example.doorweb.repositories.PersonRepository;
import com.sun.istack.Nullable;
import lombok.SneakyThrows;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Base64;
import java.util.Date;
import java.util.Optional;

@Component
public class LogCommandToLog implements Converter<LogCommand, Log> {

    private final PersonRepository personRepository;

    public LogCommandToLog(PersonRepository personRepository) {this.personRepository = personRepository;}

    @SneakyThrows
    @Synchronized
    @Nullable
    @Override
    public Log convert(LogCommand logCommand) {
        byte[] decodedImage = Base64.getDecoder().decode(logCommand.getEncodedImage());
        Byte[] image = new Byte[decodedImage.length];
        int i = 0;
        for (Byte b : decodedImage) {
            image[i] = b;
            i++;
        }

        Optional<Person> optionalPerson = personRepository.findById(Long.parseLong(logCommand.getPersonId()));
        Person person = null;
        if (optionalPerson.isPresent()) {
            person = optionalPerson.get();
        }

        Log log = new Log();
        log.setPerson(person);
        log.setImage(image);
        log.setTimestamp(Date.from(Instant.now()));

        return log;
    }
}
