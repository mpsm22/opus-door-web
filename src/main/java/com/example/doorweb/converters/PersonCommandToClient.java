package com.example.doorweb.converters;

import com.example.doorweb.commands.PersonCommand;
import com.example.doorweb.model.Client;
import com.example.doorweb.model.Employee;
import com.example.doorweb.model.Person;
import com.example.doorweb.repositories.EmployeeRepository;
import com.example.doorweb.services.AwsService;
import com.sun.istack.Nullable;
import lombok.SneakyThrows;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Component
public class PersonCommandToClient implements Converter<PersonCommand, Person> {

    private final EmployeeRepository employeeRepository;
    private final AwsService awsService;

    public PersonCommandToClient(EmployeeRepository employeeRepository,
                                 AwsService awsService) {
        this.employeeRepository = employeeRepository;
        this.awsService = awsService;
    }

    @SneakyThrows
    @Synchronized
    @Nullable
    @Override
    public Client convert(PersonCommand personCommand) {
        if (personCommand == null) {
            return null;
        }

        Client client = new Client();
        client.setFirstName(personCommand.getFirstName());
        client.setLastName(personCommand.getLastName());
        client.setPhoto(extractPhotoBytes(personCommand.getPhotoFile()));

        String[] stringTokens = personCommand.getSponsorName().split(" ");
        String sponsorFirstName = stringTokens[0];
        String sponsorLastName = stringTokens[1];
        Iterable<Employee> employeeIterable = employeeRepository.findAll();
        Employee sponsor = null;
        for (Employee employee : employeeIterable) {
            if (employee.getFirstName().equals(sponsorFirstName) &&
                    employee.getLastName().equals(sponsorLastName)) {
                sponsor = employee;
            }
        }
        client.setSponsor(sponsor);

        client.setAudioMessage(awsService.generateAudioMessage(
                client.getFirstName() + " " + client.getLastName(),
                sponsor.getFirstName() + " " + sponsor.getLastName()
        ));

        return client;
    }

    private Byte[] extractPhotoBytes(MultipartFile photoFile) throws IOException {
        Byte[] photo = new Byte[photoFile.getBytes().length];
        int i = 0;
        for (Byte b : photoFile.getBytes()) {
            photo[i] = b;
            i++;
        }
        return photo;
    }
}
