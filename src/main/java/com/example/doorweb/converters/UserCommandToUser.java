package com.example.doorweb.converters;

import com.example.doorweb.commands.UserCommand;
import com.example.doorweb.model.User;
import com.sun.istack.Nullable;
import lombok.SneakyThrows;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserCommandToUser implements Converter<UserCommand, User> {

    @SneakyThrows
    @Synchronized
    @Nullable
    @Override
    public User convert(UserCommand userCommand) {
        if (userCommand == null) {
            return null;
        }

        User user = new User();
        user.setUserType(userCommand.getUserType());
        user.setLogin(userCommand.getLogin());
        user.setPassword(userCommand.getPassword());

        return user;
    }
}
