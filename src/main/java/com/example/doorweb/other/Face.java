package com.example.doorweb.other;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Face {

    private final Long id;
    private final String name;
    private final Byte[] photo;
    private final boolean isClient;
}
