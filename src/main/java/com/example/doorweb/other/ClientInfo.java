package com.example.doorweb.other;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ClientInfo {
    
    private final Byte[] audioMessage;
    private final String sponsorEmail;
    private final String sponsorSlack;
}
