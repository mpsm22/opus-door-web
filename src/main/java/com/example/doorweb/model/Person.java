package com.example.doorweb.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.JOINED)
public class Person implements Comparable<Person> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    @Lob
    private Byte[] photo;

    @Override
    public int compareTo(Person other) {
        if (other != null) {
            return this.getFirstName().compareTo(other.getFirstName());
        } else {
            throw new NullPointerException("Null person to compare!");
        }
    }
}
