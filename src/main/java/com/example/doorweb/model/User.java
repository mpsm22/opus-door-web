package com.example.doorweb.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class User implements Comparable<User> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Enumerated(value = EnumType.STRING)
    private UserType userType;
    private String login;
    private String password;

    @Override
    public int compareTo(User other) {
        if (other != null) {
            return this.getLogin().compareTo(other.getLogin());
        } else {
            throw new NullPointerException("Null user to compare!");
        }
    }
}
