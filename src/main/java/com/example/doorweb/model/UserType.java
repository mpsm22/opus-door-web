package com.example.doorweb.model;

public enum UserType {
    ADMIN, NORMAL
}
