package com.example.doorweb.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Log implements Comparable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(cascade = CascadeType.PERSIST)
    Person person;
    @Lob
    private Byte[] image;
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    @Override
    public int compareTo(Object object) {
        if (object.getClass() != Log.class) {
            throw new IllegalArgumentException("Needs another Log to compare to a Log!");
        }
        return ((Log) object).getTimestamp().compareTo(this.timestamp);
    }
}
