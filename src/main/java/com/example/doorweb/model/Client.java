package com.example.doorweb.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Client extends Person {
    @ManyToOne(cascade = CascadeType.PERSIST)
    private Employee sponsor;
    @Lob
    private Byte[] audioMessage;
}
