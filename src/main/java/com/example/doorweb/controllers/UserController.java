package com.example.doorweb.controllers;

import com.example.doorweb.commands.UserCommand;
import com.example.doorweb.model.User;
import com.example.doorweb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("user")
    public String mainPage() {
        return "user/main-page";
    }

    @RequestMapping("/user/list-page")
    public String listPage(Model model) {
        List<User> userList = userService.getAll();
        model.addAttribute("userList", userList);
        return "user/list-page";
    }

    @RequestMapping("/user/insert-page")
    public String insertPage(Model model) {
        model.addAttribute("userCommand", new UserCommand());
        return "user/insert-page";
    }

    @RequestMapping("/user/delete-page")
    public String deletePage(Model model) {
        List<User> userList = userService.getAll();
        model.addAttribute("userList", userList);
        return "user/delete-page";
    }

    @PostMapping("/user/insert")
    public String insert(@ModelAttribute UserCommand userCommand) {
        userService.saveCommand(userCommand);
        return "redirect:/user/insert-page";
    }

    @GetMapping("/user/delete/{id}")
    public String delete(@PathVariable String id) {
        userService.remove(Long.parseLong(id));
        return "redirect:/user/delete-page";
    }
}
