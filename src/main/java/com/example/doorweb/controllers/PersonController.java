package com.example.doorweb.controllers;

import com.example.doorweb.commands.PersonCommand;
import com.example.doorweb.model.Client;
import com.example.doorweb.model.Employee;
import com.example.doorweb.model.Log;
import com.example.doorweb.model.Person;
import com.example.doorweb.repositories.LogRepository;
import com.example.doorweb.services.HttpService;
import com.example.doorweb.services.PersonService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Set;

@Controller
public class PersonController {

    private final PersonService personService;
    private final HttpService httpService;
    private final LogRepository logRepository;

    public PersonController(PersonService personService,
                            HttpService httpService,
                            LogRepository logRepository) {
        this.personService = personService;
        this.httpService = httpService;
        this.logRepository = logRepository;
    }

    @Autowired


    @RequestMapping("/person")
    public String mainPage() {
        return "person/main-page";
    }

    @RequestMapping("/person/list-page")
    public String listPage(Model model) {
        List<Employee> employeeList = personService.getAllEmployee();
        model.addAttribute("employeeList", employeeList);
        List<Client> clientList = personService.getAllClient();
        model.addAttribute("clientList", clientList);
        return "person/list-page";
    }

    @RequestMapping("/person/insert-page")
    public String insertPage(Model model) {
        model.addAttribute("personCommand", new PersonCommand());
        return "person/insert-page";
    }

    @RequestMapping("/person/delete-page")
    public String deletePage(Model model) {
        List<Employee> employeeList = personService.getAllEmployee();
        model.addAttribute("employeeList", employeeList);
        List<Client> clientList = personService.getAllClient();
        model.addAttribute("clientList", clientList);
        return "person/delete-page";
    }

    @PostMapping("/person/insert")
    public String insert(@ModelAttribute PersonCommand personCommand) {
        Person savedPerson = personService.saveCommand(personCommand);
        httpService.addRaspFace(savedPerson);
        return "redirect:/person/insert-page";
    }

    @GetMapping("/person/delete/{id}")
    public String delete(@PathVariable String id) {
        Long idLong = Long.parseLong(id);
        Iterable<Log> logSet = logRepository.findAll();
        for (Log log : logSet) {
            if (log.getPerson() != null) {
                if (log.getPerson().getId().equals(idLong)) {
                    log.setPerson(null);
                    logRepository.save(log);
                }
            }
        }
        Person removedPerson = personService.remove(idLong);
        httpService.removeRaspFace(removedPerson);
        return "redirect:/person/delete-page";
    }

    @RequestMapping("/person/photo/{id}")
    public void photo(@PathVariable String id, HttpServletResponse response) throws IOException {
        Person person = personService.get(Long.parseLong(id));
        if (person.getPhoto() != null) {
            Byte[] photo = person.getPhoto();
            byte[] photoPrimitive = new byte[photo.length];
            int i = 0;
            for (Byte b : photo) {
                photoPrimitive[i] = b;
                i++;
            }

            response.setContentType("image/jpeg");
            InputStream inputStream = new ByteArrayInputStream(photoPrimitive);
            IOUtils.copy(inputStream, response.getOutputStream());
        }
    }

}
