package com.example.doorweb.controllers;

import com.example.doorweb.commands.UserCommand;
import com.example.doorweb.model.Log;
import com.example.doorweb.model.Person;
import com.example.doorweb.model.UserType;
import com.example.doorweb.services.LogService;
import com.example.doorweb.services.PersonService;
import com.example.doorweb.services.UserService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Set;

@Controller
public class WebController {

    private final UserService userService;
    private final LogService logService;
    private final PersonService personService;

    @Autowired
    public WebController(UserService userService,
                         LogService logService,
                         PersonService personService) {
        this.userService = userService;
        this.logService = logService;
        this.personService = personService;
    }

    @RequestMapping("")
    public String loginPage(Model model) {
        model.addAttribute("userCommand", new UserCommand());
        return "login-page";
    }

    @PostMapping("login")
    public String login(@ModelAttribute UserCommand userCommand) {
        UserType verifiedUserType = userService.verify(userCommand);
        if (verifiedUserType == UserType.ADMIN) {
            return "redirect:/index-page";
        } else if (verifiedUserType == UserType.NORMAL) {
            return "redirect:/person-page";
        } else {
            return "redirect:/";
        }
    }

    @RequestMapping("index-page")
    public String indexPage() {
        return "index-page";
    }

    @RequestMapping("log-page")
    public String logPage(Model model) {
        List<Log> logList = logService.getAll();
        model.addAttribute("logList", logList);
        return "log-page";
    }

    @RequestMapping("person-page")
    public String personPage(Model model) {
        List<Person> personList = personService.getAll();
        model.addAttribute("personList", personList);
        return "person-page";
    }

    @RequestMapping("log-image/{id}")
    public void photo(@PathVariable String id, HttpServletResponse response) throws IOException {
        Log log = logService.get(Long.parseLong(id));
        if (log.getImage() != null) {
            Byte[] image = log.getImage();
            byte[] imagePrimitive = new byte[image.length];
            int i = 0;
            for (Byte b : image) {
                imagePrimitive[i] = b;
                i++;
            }

            response.setContentType("image/jpeg");
            InputStream inputStream = new ByteArrayInputStream(imagePrimitive);
            IOUtils.copy(inputStream, response.getOutputStream());
        }
    }
}
