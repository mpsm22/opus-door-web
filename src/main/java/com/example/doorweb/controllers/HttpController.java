package com.example.doorweb.controllers;

import com.example.doorweb.commands.LogCommand;
import com.example.doorweb.other.ClientInfo;
import com.example.doorweb.other.Face;
import com.example.doorweb.services.LogService;
import com.example.doorweb.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
public class HttpController {

    private final PersonService personService;
    private final LogService logService;

    @Autowired
    public HttpController(PersonService personService,
                          LogService logService) { this.personService = personService;
        this.logService = logService;
    }

    @GetMapping("/faces")
    public Set<Face> faces() {
        return personService.getAllAsFace();
    }

    @GetMapping("/client/{id}")
    public ClientInfo client(@PathVariable String id) {
        return personService.getClientInfo(Long.parseLong(id));
    }

    @PostMapping("/log")
    public void log(@RequestBody LogCommand logCommand) {
        logService.saveCommand(logCommand);
    }
}
