package com.example.doorweb.services;

import com.example.doorweb.commands.PersonCommand;
import com.example.doorweb.converters.PersonCommandToClient;
import com.example.doorweb.converters.PersonCommandToEmployee;
import com.example.doorweb.model.Client;
import com.example.doorweb.model.Employee;
import com.example.doorweb.other.ClientInfo;
import com.example.doorweb.other.Face;
import com.example.doorweb.model.Person;
import com.example.doorweb.repositories.ClientRepository;
import com.example.doorweb.repositories.EmployeeRepository;
import com.example.doorweb.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;
    private final ClientRepository clientRepository;
    private final EmployeeRepository employeeRepository;
    private final PersonCommandToClient personCommandToClient;
    private final PersonCommandToEmployee personCommandToEmployee;

    @Autowired
    public PersonServiceImpl(PersonRepository personRepository,
                             ClientRepository clientRepository,
                             EmployeeRepository employeeRepository,
                             PersonCommandToClient personCommandToClient,
                             PersonCommandToEmployee personCommandToEmployee) {
        this.personRepository = personRepository;
        this.clientRepository = clientRepository;
        this.employeeRepository = employeeRepository;
        this.personCommandToClient = personCommandToClient;
        this.personCommandToEmployee = personCommandToEmployee;
    }

    @Override
    public Person get(Long id) {
        Optional<Person> optionalPerson = personRepository.findById(id);
        if (optionalPerson.isPresent()) {
            return optionalPerson.get();
        } else {
            return null;
        }
    }

    @Override
    public List<Person> getAll() {
        Iterable<Person> personIterable = personRepository.findAll();
        List<Person> personList = new ArrayList<>();
        personIterable.forEach((person) -> personList.add(person));
        Collections.sort(personList);
        return personList;
    }

    @Transactional
    @Override
    public Person saveCommand(PersonCommand personCommand) {

        Person person;
        if (personCommand.getEmail() != null) { // employee
            person = employeeRepository.save(personCommandToEmployee.convert(personCommand));
        } else { // client
            person = clientRepository.save(personCommandToClient.convert(personCommand));
        }
        return person;
    }

    @Override
    public Person remove(Long id) {
        Optional<Person> optionalPerson = personRepository.findById(id);
        personRepository.deleteById(id);
        return optionalPerson.get();
    }

    @Override
    public Set<Face> getAllAsFace() {
        Set<Face> faceSet = new HashSet<>();
        Iterable<Person> personIterable = personRepository.findAll();
        for (Person person : personIterable) {
            Optional<Client> optionalClient = clientRepository.findById(person.getId());
            Face face = new Face(
                    person.getId(),
                    person.getFirstName() + " " + person.getLastName(),
                    person.getPhoto(),
                    optionalClient.isPresent());
            faceSet.add(face);
        }
        return faceSet;
    }

    @Override
    public ClientInfo getClientInfo(Long id) {
        Optional<Client> optionalClient = clientRepository.findById(id);
        if (optionalClient.isPresent()) {
            Client client = optionalClient.get();
            Employee sponsor = client.getSponsor();
            ClientInfo clientInfo = new ClientInfo(
                    client.getAudioMessage(),
                    sponsor.getEmail(),
                    sponsor.getSlack());
            return clientInfo;

        } else {
            return null;
        }
    }

    @Override
    public List<Employee> getAllEmployee() {
        Iterable<Employee> employeeIterable = employeeRepository.findAll();
        List<Employee> employeeList = new ArrayList<>();
        employeeIterable.forEach((person) -> employeeList.add(person));
        Collections.sort(employeeList);
        return employeeList;
    }

    @Override
    public List<Client> getAllClient() {
        Iterable<Client> clientIterable = clientRepository.findAll();
        List<Client> clientList = new ArrayList<>();
        clientIterable.forEach((person) -> clientList.add(person));
        Collections.sort(clientList);
        return clientList;
    }
}
