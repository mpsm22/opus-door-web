package com.example.doorweb.services;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.polly.AmazonPolly;
import com.amazonaws.services.polly.AmazonPollyClientBuilder;
import com.amazonaws.services.polly.model.*;
import com.amazonaws.util.IOUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class AwsServiceImpl implements AwsService {

    private final AmazonPolly polly;
    private final Voice voice;

    AwsServiceImpl() {
        AWSCredentials awsCredentials = new BasicAWSCredentials(
                System.getenv("AWS_ACCESS"),
                System.getenv("AWS_SECRET"));
        AWSCredentialsProvider awsCredentialsProvider = new AWSStaticCredentialsProvider(awsCredentials);
        polly = AmazonPollyClientBuilder.standard().
                withCredentials(awsCredentialsProvider).
                withRegion(Regions.US_WEST_2).
                build();

        DescribeVoicesRequest describeVoicesRequest = new DescribeVoicesRequest().
                withLanguageCode(LanguageCode.PtBR).
                withEngine(Engine.Neural);
        DescribeVoicesResult describeVoicesResult = polly.describeVoices(describeVoicesRequest);
        voice = describeVoicesResult.getVoices().get(0);
    }

    @Override
    public Byte[] generateAudioMessage(String clientName, String sponsorName) throws IOException {
        String message = "<speak>" +
                "Olá " + clientName + "!" +
                " Uma notificação de sua chegada foi enviada para " + sponsorName +
                "<break time=\"1s\"/></speak>";

        SynthesizeSpeechRequest synthRequest =
                new SynthesizeSpeechRequest().
                        withText(message).
                        withTextType(TextType.Ssml).
                        withVoiceId(voice.getId()).
                        withOutputFormat(OutputFormat.Ogg_vorbis);
        SynthesizeSpeechResult synthResult = polly.synthesizeSpeech(synthRequest);

        byte[] bytesPrimitive = IOUtils.toByteArray(synthResult.getAudioStream());
        Byte[] audioMessage = new Byte[bytesPrimitive.length];
        int i = 0;
        for (Byte b : bytesPrimitive) {
            audioMessage[i] = b;
            i++;
        }

        return audioMessage;
    }
}
