package com.example.doorweb.services;

import com.example.doorweb.commands.PersonCommand;
import com.example.doorweb.model.Client;
import com.example.doorweb.model.Employee;
import com.example.doorweb.other.ClientInfo;
import com.example.doorweb.other.Face;
import com.example.doorweb.model.Person;

import java.util.List;
import java.util.Set;

public interface PersonService {

    Person get(Long id);
    List<Person> getAll();
    Person saveCommand(PersonCommand personCommand);
    Person remove(Long id);
    Set<Face> getAllAsFace();
    ClientInfo getClientInfo(Long id);
    List<Employee> getAllEmployee();
    List<Client> getAllClient();
}
