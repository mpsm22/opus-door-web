package com.example.doorweb.services;

import com.example.doorweb.model.Client;
import com.example.doorweb.model.Person;
import com.example.doorweb.other.Face;
import com.example.doorweb.repositories.ClientRepository;
import com.example.doorweb.repositories.PersonRepository;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.util.Collections;
import java.util.Optional;

@Service
public class HttpServiceImpl implements HttpService {

    private final RestTemplate restTemplate;
    private final PersonRepository personRepository;
    private final ClientRepository clientRepository;

    public HttpServiceImpl(RestTemplateBuilder restTemplateBuilder,
                           PersonRepository personRepository,
                           ClientRepository clientRepository) {

        this.restTemplate = restTemplateBuilder.setConnectTimeout(Duration.ofMillis(500)).build();
        this.personRepository = personRepository;
        this.clientRepository = clientRepository;
    }

    @Override
    public void addRaspFace(Person person) {
        String url = "http://192.168.0.101:5000/add-rasp-face";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity<Face> httpEntity = new HttpEntity<>(getFace(person), headers);
        try {
            this.restTemplate.postForEntity(url, httpEntity, String.class);
        } catch (ResourceAccessException e) { }
    }

    @Override
    public void removeRaspFace(Person person) {
        String url = "http://192.168.0.101:5000/rem-rasp-face";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity<Face> httpEntity = new HttpEntity<>(getFace(person), headers);
        try {
            this.restTemplate.postForEntity(url, httpEntity, String.class);
        } catch (ResourceAccessException e) { }
    }

    private Face getFace(Person person) {
        Optional<Client> optionalClient = clientRepository.findById(person.getId());
        Face face = new Face(
                person.getId(),
                person.getFirstName() + " " + person.getLastName(),
                person.getPhoto(),
                optionalClient.isPresent()
        );
        return face;
    }
}
