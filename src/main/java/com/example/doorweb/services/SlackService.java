package com.example.doorweb.services;

import com.slack.api.methods.SlackApiException;

import java.io.IOException;

public interface SlackService {

    String getUserId(String email) throws IOException, SlackApiException;
}
