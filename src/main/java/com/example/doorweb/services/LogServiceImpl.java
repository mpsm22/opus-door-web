package com.example.doorweb.services;

import com.example.doorweb.commands.LogCommand;
import com.example.doorweb.converters.LogCommandToLog;
import com.example.doorweb.model.Log;
import com.example.doorweb.repositories.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class LogServiceImpl implements LogService {

    private final LogRepository logRepository;
    private final LogCommandToLog logCommandToLog;

    @Autowired
    public LogServiceImpl(LogRepository logRepository,
                          LogCommandToLog logCommandToLog) {
        this.logRepository = logRepository;
        this.logCommandToLog = logCommandToLog;
    }

    @Override
    public List<Log> getAll() {
        Iterable<Log> logIterable = logRepository.findAll();
        List<Log> logList = new ArrayList<>();
        logIterable.forEach((log) -> logList.add(log));
        Collections.sort(logList);
        return logList;
    }

    @Override
    public void saveCommand(LogCommand logCommand) {
        if (logRepository.count() >= 50) {
            Iterable<Log> logIterable = logRepository.findAll();
            Log olderLog = logIterable.iterator().next();
            for (Log log : logIterable) {
                if (log.compareTo(olderLog) > 0) {
                    olderLog = log;
                }
            }
            logRepository.deleteById(olderLog.getId());
        }
        logRepository.save(logCommandToLog.convert(logCommand));
    }

    @Override
    public Log get(Long id) {
        Optional<Log> logOptional = logRepository.findById(id);
        if (logOptional.isPresent()) {
            return logOptional.get();
        } else {
            return null;
        }
    }
}
