package com.example.doorweb.services;

import com.slack.api.Slack;
import com.slack.api.methods.MethodsClient;
import com.slack.api.methods.SlackApiException;
import com.slack.api.methods.request.users.UsersLookupByEmailRequest;
import com.slack.api.methods.response.users.UsersLookupByEmailResponse;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class SlackServiceImpl implements SlackService {

    private MethodsClient client;

    public SlackServiceImpl() {
        client = Slack.getInstance().methods(System.getenv("SLACK_TOKEN"));
    }

    @Override
    public String getUserId(String email) throws IOException, SlackApiException {
        UsersLookupByEmailRequest request = UsersLookupByEmailRequest.builder().email(email).build();
        UsersLookupByEmailResponse response = client.usersLookupByEmail(request);
        if (response.isOk()) {
            return response.getUser().getId();
        } else {
            return null;
        }
    }
}
