package com.example.doorweb.services;

import com.example.doorweb.commands.UserCommand;
import com.example.doorweb.converters.UserCommandToUser;
import com.example.doorweb.model.User;
import com.example.doorweb.model.UserType;
import com.example.doorweb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAll() {
        Iterable<User> userIterable = userRepository.findAll();
        List<User> userList = new ArrayList<>();
        userIterable.forEach((user) -> userList.add(user));
        Collections.sort(userList);
        return userList;
    }

    @Transactional
    @Override
    public User saveCommand(UserCommand userCommand) {
        UserCommandToUser userCommandToUser = new UserCommandToUser();
        User user = userCommandToUser.convert(userCommand);
        return userRepository.save(user);
    }

    @Override
    public void remove(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public UserType verify(UserCommand userCommand) {
        Iterable<User> userIterable = userRepository.findAll();
        for (User user : userIterable) {
            Boolean equalUser = userCommand.getLogin().equals(user.getLogin());
            Boolean equalPass = userCommand.getPassword().equals(user.getPassword());
            if (equalUser && equalPass) {
                return user.getUserType();
            }
        }
        return null;
    }
}
