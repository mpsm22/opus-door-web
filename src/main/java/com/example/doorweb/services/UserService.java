package com.example.doorweb.services;

import com.example.doorweb.commands.UserCommand;
import com.example.doorweb.model.User;
import com.example.doorweb.model.UserType;

import java.util.List;
import java.util.Set;

public interface UserService {

    List<User> getAll();
    User saveCommand(UserCommand userCommand);
    void remove(Long id);
    UserType verify(UserCommand userCommand);
}
