package com.example.doorweb.services;

import com.example.doorweb.commands.LogCommand;
import com.example.doorweb.model.Log;

import java.util.List;
import java.util.Set;

public interface LogService {

    List<Log> getAll();
    void saveCommand(LogCommand logCommand);
    Log get(Long id);
}
