package com.example.doorweb.services;


import java.io.IOException;

public interface AwsService {
    public Byte[] generateAudioMessage(String clientName, String sponsorName) throws IOException;
}
